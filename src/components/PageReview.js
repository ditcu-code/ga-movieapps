import React, {useState, useEffect} from "react";
import "../assets/css/pageoverview.scss";
import {useParams} from "react-router-dom";
import {reviewList} from "../stores/actions/review";
import {useDispatch, useSelector} from "react-redux";
import {postReview} from "../stores/actions/review";
import {
    Comment,
    Avatar,
    Form,
    Input,
    Button,
    Rate
} from "antd";
import "../../node_modules/antd/dist/antd.css"
// import { DislikeOutlined, LikeOutlined, DislikeFilled, LikeFilled } from '@ant-design/icons';

const PageReview = () => {
    let token = localStorage.getItem("token")
    let {id} = useParams()
    const dispatch = useDispatch()
    const [review, setReview] = useState({
        comment: "",
        rating: null
    })
    const data = review
    const reviews= useSelector(state => state.review.comments)
    const [value, setValue] = useState()
    
    // console.log("komen", reviews)

    const handleChange = value => {
        // dispatch(postRating(id, rating))
        console.log(value)
        setValue(value)
        setReview({rating: value})
    }

    console.log(id, data)
    useEffect(() => {
        dispatch(reviewList(id))
    }, [dispatch, id])
    
    const handleInput = e => {
        setReview({
            ...review,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = () => {
        handleError()
        dispatch(postReview(id, data))
        setTimeout(() => {
            setReview({comment:""})
            dispatch(reviewList(id))
        }, 3000);
    }

    const handleError = () => {
        if (token===null) {
            alert("please sign in first")
        }
    }
    return(
        <div>
            <Form>
                <Form.Item
                    name="comment"
                    // rules={[{ required: true, message: 'Please input your review!' }]}
                >
                    <Rate value={value} onChange={handleChange} />
                    <Input placeholder="Add your review..." allowClear value={review.comment}  name="comment" onChange={handleInput} />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" onClick={handleSubmit}>Submit</Button>
                </Form.Item>
            </Form>
            <div>
                {reviews.length ?
                    reviews.map(item => 
                        <Comment
                            author={<p style={{color:"#858585"}}>{item.User.name}</p>}
                            avatar={
                                <Avatar size='large'
                                src={item.User.Image.url}
                                alt="user"
                                />
                            }
                            content={
                                <div>
                                    <Rate disabled value={item.rating}/>
                                    <p>"{item.comment}"</p>
                                </div>
                                  
                            }
                        />
                    )
                    :
                    <p>No reviews yet.</p>
                } 
            </div>
        </div>
    )
}

export default PageReview;