import {GET_PROFILE, GET_WATCHLIST, GET_MYREVIEW} from "../actions/types";

const initialState= {
    profile: null,
    watchlist: [],
    myreview: []
}

const userdata = (state = initialState, action) => {
    switch(action.type) {
        case GET_PROFILE:
            return {
            ...state,
            profile: action.payload
            }
        case GET_WATCHLIST:
            return {
            ...state,
            watchlist: action.payload
            }
        case GET_MYREVIEW:
            return {
            ...state,
            myreview: action.payload
            }
        default:
        return{
            ...state
        }
    }
}

export default userdata;