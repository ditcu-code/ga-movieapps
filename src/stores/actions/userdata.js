import {GET_PROFILE, GET_WATCHLIST, GET_MYREVIEW, DEL_WATCHLIST} from "./types";
import axios from "axios";
const baseUrl = "https://ga-moviereview.herokuapp.com/api/v1";

export const getProfile = () => async dispatch => {
    let token = localStorage.getItem("token")
    try{
        const res = await axios.get(`${baseUrl}/user`, {
            headers: {
                auth: token
            }
        })
        // console.log('getprofile', res.data.data)
        dispatch({
            type: GET_PROFILE,
            payload: res.data.data
        })
    }catch(error){
        console.log(error, error.response)
    }
}

export const getWatchlist = () => async dispatch => {
    let token = localStorage.getItem("token")
    try{
        const res = await axios.get(`${baseUrl}/movie/watchlist`, {
            headers: {
                auth: token
            }
        })
        dispatch({
            type: GET_WATCHLIST,
            payload: res.data.data
        })
    }catch(error){
        console.log(error, error.response)
    }
}

export const getMyReview = () => async dispatch => {
    console.log('getMyReview0')
    let token = localStorage.getItem("token")
    try{
        const res = await axios.get(`${baseUrl}/review/owned`, {
            headers: {
                auth: token
            }
        })
        console.log('getMyReview1', res.data.data.rows)
        dispatch({
            type: GET_MYREVIEW,
            payload: res.data.data.rows
        })
    }catch(error){
        console.log(error, error.response)
    }
}

export const delWatchlist = (key) => async dispatch => {
    let token = localStorage.getItem("token")
    console.log('delWatchlist0', key, token)
    try{
        const res = await axios.delete(`${baseUrl}/movie/watchlist/${key}`, {
            headers: {
                auth: token
            }
        })
        console.log('delWatchlist1', res.data.data)
        dispatch({
            type: DEL_WATCHLIST,
            payload: res.data.data
        })
    }catch(error){
        console.log(error, error.response)
    }
}

export const delReview = id => async dispatch => {
    let token = localStorage.getItem("token")
    console.log('delReview', id, token)
    try{
        const res = await axios.delete(`${baseUrl}/review/${id}`, {
            headers: {
                auth: token
            }
        })
        console.log('delReview2', res.data.data)
        dispatch({
            type: 'DEL_REVIEW',
            payload: res.data.data
        })
    }catch(error){
        console.log(error, error.response)
    }
}
  
export const addWatchlist = id => async dispatch => {
    const token = localStorage.getItem("token");
    console.log(id, token);
    try {
      const res = await fetch(`${baseUrl}/movie/watchlist/${id}`, {
        method: "POST",
        headers: {
          auth: token,
        },
        body: JSON.stringify(id)
      });
      await res.json();
      console.log('ADDED TO WATCHLIST')
    } catch (error) {
      dispatch({
        type: "FAILED_REVIEW",
        payload: error
      });
      console.log(error);
    }
  };
  
  export const putReview = (id, data) => async dispatch => {
    let token = localStorage.getItem("token")
    console.log('putReview', id, data, token)
    try{
        const res = await axios.delete(`${baseUrl}/review/${id}`, data, {
            headers: {
                auth: token
            }
        })
        console.log('putReview2', res)
        dispatch({
            type: 'PUT_REVIEW',
            payload: res
        })
    }catch(error){
        console.log(error, error.response)
    }
}
  
