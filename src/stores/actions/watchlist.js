const baseUrl = 'https://ga-moviereview.herokuapp.com/api/v1';

export const addWatchlist = id => async dispatch => {
    let token = localStorage.getItem("token")
    console.log(id, token)
    try {
        const res = await fetch(`${baseUrl}/movie/watchlist/${id}`, {
            method: "POST",
            headers: {
                auth: token
            }
        })
        console.log("watchlist", res)
    }catch(error){
        console.log(error)
    }
}