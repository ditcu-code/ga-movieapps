import React, { useState, useEffect} from "react";
import { useSelector, useDispatch } from "react-redux";
import {Link} from "react-router-dom";
import axios from "axios";
import { editProfile } from "../stores/actions/updateprofile";
import { UserOutlined,  DeleteOutlined, ExpandAltOutlined, EditOutlined} from '@ant-design/icons';
import { getWatchlist, getMyReview, delWatchlist, delReview} from "../stores/actions/userdata";
import { Layout, Menu, Row, Col, Avatar, Card, Modal, Upload, Input, Progress, Tooltip, Rate, Typography} from 'antd';
import "../assets/css/profile.scss";
const token = localStorage.getItem('token');
const { Text } = Typography;


const ProfilePage = () => {
    const dispatch = useDispatch()
    const watchlist = useSelector(state => state.userdata.watchlist)
    const reviewlist = useSelector(state => state.userdata.myreview)
    const userdata = useSelector(state => state.userdata.profile)
    const [current, setCurrent] = useState('two')
    const [deleted, setdeleted] = useState(false)
    const [progress, setProgress] = useState(0);
    const [name, setname] = useState()
    const { Content, Sider } = Layout;
    const [showModalProfile, setShowModalProfile] = useState(false)
    const [defaultFileList, setDefaultFileList] = useState([]);
    const {Meta} = Card;

    
    const handleOnChange = ({ file, fileList, event }) => {
        setDefaultFileList(fileList);
      };
      
    useEffect(() => {
        if (watchlist.length <= 0) {
            console.log('userdata', userdata)
            console.log('watchlist', watchlist)
            dispatch(getWatchlist())
        }
        if (reviewlist.length === 0) {
            console.log('reviewdispatch', reviewlist)
            dispatch(getMyReview())            
        }
        if (deleted === true){
            dispatch(getWatchlist())
            dispatch(getMyReview())            
            setTimeout(() => {
                console.log('timer')
                setdeleted(false)
            }, 3000);
        }
    }, [watchlist, reviewlist.length, deleted, userdata, dispatch, reviewlist])
  
    const uploadImage = async options => {
        const { onSuccess, onError, file, onProgress } = options;
        const fmData = new FormData();
        const config = {
            headers: { auth: token },
            onUploadProgress: event => {
            const percent = Math.floor((event.loaded / event.total) * 100);
            setProgress(percent);
            if (percent === 100) {
                setTimeout(() => setProgress(0), 1000);
            }
            onProgress({ percent: (event.loaded / event.total) * 100 });
            }
        };
        fmData.append("image", file);
        try {
            const res = await axios.put(
            "https://ga-moviereview.herokuapp.com/api/v1/user",
            fmData,
            config
            );
            onSuccess("Ok");
            console.log("server res: ", res);
        } catch (err) {
            console.log("Eroor: ", err);
            const error = new Error("Some error");
            onError({ error });
        }
    };  
    
    const handleInput = e => {
        setname({
            ...name,
            [e.target.name]: e.target.value
        })
    }
    // const handleEdit = e => {
    //     setReview({
    //         ...review,
    //         [e.target.name]: e.target.value
    //     })
    // }
    // const handleSubmit = (id, data) => {
    //     console.log('submit', id, data)
    //     // dispatch(putReview(id, data))
    //     // setTimeout(() => {
    //     //     setReview({comment:""})
    //     //     dispatch(reviewList(id))
    //     // }, 3000);
    // }
    const handleUpdate = e => {
        dispatch(editProfile(name))
    }
    // const handleChange = value => {
    //     // dispatch(postRating(id, rating))
    //     console.log(value)
    //     setValue(value)
    //     setReview({rating: value})
    // }


    // const confirm = (e) => {
    //     console.log(e);
    //     message.success('Click on Yes');
    //   }
      
    // const cancel = (e) => {
    //     console.log(e);
    //     message.error('Click on No');
    // }

    const call = (key) => {
        console.log(key)
        dispatch(delWatchlist(key))
        setdeleted(true)
    }

    const dell = (key) => {
        console.log(key)
        dispatch(delReview(key))
        setdeleted(true)
    }
    
    const watchlistItem = watchlist.map(item => 
        <Card key={item.id}
        cover={<img alt={item.Movie.title}
        src={item.Movie.Images[0].url}
        />}
        actions={[
            <Link to={`/overview/${item.Movie.id}`} >
            <ExpandAltOutlined key='visit' style={{ fontSize: '20px'}} />
            </Link>,
            <DeleteOutlined key="delete" style={{ fontSize: '20px' }} onClick={() => call(item.id)}  />
        ]}
        >
              <Meta title={item.Movie.title} />
          </Card>
    );

    const reviewlistItem = reviewlist.map(item =>
        <Col span={24} key={item.id}>
        <Card  className='ant-card-reviewlist'
        style={{ width: 300 }}
        cover={
          <img
            alt={item.Movie.title}
            src={item.Movie.Images[2].url}
          />
        }
        actions={[
          <ExpandAltOutlined key="visit" style={{fontSize:'20px'}}/>,
          <EditOutlined key="edit" style={{fontSize:'20px'}} />,
          <DeleteOutlined key="delete" style={{fontSize:'20px'}} onClick={() => dell(item.id)} />,
        ]}
        >
        <Card type='inner' className='ant-card-inner'>
        <Meta style={{paddingLeft:'30px'}}
          title={item.Movie.title}
          avatar={<Rate disabled defaultValue={item.rating} />}
          />
          <br/>
          <Text style={{fontSize:'24px', color:'#858585'}} >"{item.comment}"</Text>
          </Card>
        </Card>
        </Col>
    );


    const content1 = (
            <Content style={{ paddingLeft: '10px', minHeight: 280, background:"#262626", color:"#858585" }}>
                <h2 style={{color:"#858585", paddingBottom:'10px'}}>My Watchlist</h2>
                <Row>
                    {watchlistItem}
                </Row>
            </Content>
    )

    const content2 = (
        <Content style={{ paddingLeft: '10px', minHeight: 280, background:"#262626", color:"#858585" }}>
            <h2 style={{color:"#858585", paddingBottom:'10px'}}>My Review</h2>
            <Row>
                {reviewlistItem}
            </Row>
        </Content>
    )

    return(
        <div>
            <Modal
                title="Update Profile"
                visible={showModalProfile}
                onOk={() => handleUpdate()}
                onCancel={() => setShowModalProfile(false)}
            >
                {/* <UpdateProfile/> */}
                <Row style={{marginBottom:'20px'}}>
                    <p>Usernames</p>
                    <Input name='name' placeholder="Input new username" onChange={handleInput} />
                </Row>
                    <p>Update profile photo</p>
                    <div class="container">
                        <Upload
                        accept="image/*"
                        customRequest={uploadImage}
                        onChange={handleOnChange}
                        listType="picture-card"
                        defaultFileList={defaultFileList}
                        className="image-upload-grid"
                        >
                        {defaultFileList.length >= 1 ? null : <div>Upload Button</div>}
                        </Upload>
                        {progress > 0 ? <Progress percent={progress} /> : null}
                    </div>
            </Modal>
            <Layout>
                <Content style={{ padding: '0 50px', background:"#191919" }}>
                <Layout className="site-layout-background main-section-wrapper" style={{ padding: '24px 0', background:"#262626", color:"#858585" }}>
                    <Sider className="site-layout-background" width={200} style={{height: "fit-content", background:"#262626"}}>
                        <Col style={{textAlign:"center"}}>
                            <Tooltip title="Click to edit profile" placement='right'>
                                <Avatar size={80} src={userdata? userdata.image : <UserOutlined/>} onClick={() => setShowModalProfile(true)} className="profile-photo" />
                            </Tooltip>
                             <p style={{paddingTop: '10px', fontSize:'18px', padding:'12px 10px 0px 10px'}}>{userdata? userdata.name : ""}</p>
                        </Col>
                        <Menu
                            mode="inline"
                            style={{ height: '100%', background:"#262626", color:"#858585" }}
                            onClick={(e) => setCurrent(e.key)}
                            selectedKeys={[current]}
                        >
                            <Menu.Item key="one">My Watchlist</Menu.Item>
                            <Menu.Item key="two">My Review</Menu.Item>
                        </Menu>
                    </Sider>
                        <Col className={current==='two' ? 'ant-card-reviewlist' : 'ant-card-watchlist'} >
                        {/* <Popconfirm
                            title="Are you sure delete this task?"
                            onConfirm={confirm}
                            onCancel={cancel}
                            okText="Yes"
                            cancelText="No"
                        >
                            <a href="/">
                            </a>
                        </Popconfirm> */}
                                {current==='two' ? (content2) : current==='one' ? (content1) : 'Welcome'}
                        </Col>
                        {/* <Col className='movieDetail' >
                            <Content style={{ padding: '0 24px', minHeight: 280 }}>Movie Details</Content>
                        </Col> */}
                </Layout> 
                </Content>
            </Layout>
            {/* {updateProfile} */}
        </div>
    )
}
export default ProfilePage;